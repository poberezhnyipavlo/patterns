<?php

namespace App\Http\Controllers\BehavioralPatterns;

use App\DesignPaters\Behavioral\Strategy\SalaryManager;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class StrategyController extends Controller
{
    /**
     * Handle the incoming request.
     * @param Request $request
     * @return Application|Factory|View
     */
    public function __invoke(Request $request)
    {
        $name = ' Стратегія ';

        $period = [
            Carbon::now()->subMonth()->startOfMonth(),
            Carbon::now()->subMonth()->endOfMonth(),
        ];

        $users = User::all();

        $result = (new SalaryManager($period, $users))->handle();

        \DebugBar::info($result);

        return view('dump', compact('name'));
    }
}

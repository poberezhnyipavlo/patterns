<?php

namespace App\DesignPaters\Behavioral\Strategy;

use App\DesignPaters\Behavioral\Strategy\Interfaces\SalaryStrategyInterface;
use App\Models\User;
use Illuminate\Support\Collection;

class SalaryManager
{
    private SalaryStrategyInterface $salaryStrategy;

    private array $period;

    private Collection $users;

    /**
     * SalaryManager constructor.
     * @param array $period
     * @param Collection $users
     */
    public function __construct(array $period, Collection $users)
    {
        $this->period = $period;
        $this->users = $users;
    }

    /**
     * @return Collection
     */
    public function handle()
    {
        $userSalary = $this->calculateSalary();

        $this->saveSalary($userSalary);

        return $userSalary;
    }

    /**
     * @return Collection
     */
    private function calculateSalary(): Collection
    {
        $usersSalary = $this->users->map(
            function (User $user) {
                $strategy = $this->getStrategyByUser($user);
                $salary = $this
                    ->setCalculatesStrategy($strategy)
                    ->calculateUserSalary($this->period, $user);

                $strategyName = $this->strategy->getName();
                $userId = $user->id;

                $newItem = compact('userId', 'salary', 'strategyName');

                return $newItem;
            }
        );

        return $usersSalary;
    }

    /**
     * @param User $user
     * @return SalaryStrategyInterface
     * @throws \Throwable
     */
    private function getStrategyByUser(User $user): SalaryStrategyInterface
    {
        $strategyName = $user->departmentName() . 'Strategy';
        $strategyClass = __NAMESPACE__ . '\\Strategies\\' . ucwords($strategyName);

        throw_if(!class_exists($strategyClass), \Exception::class,
        "Клас не існує[$strategyClass]");

        return new $strategyClass;
    }

    /**
     * @param SalaryStrategyInterface $strategy
     * @return SalaryManager
     */
    private function setCalculatesStrategy(SalaryStrategyInterface $strategy): SalaryManager
    {
        $this->salaryStrategy = $strategy;

        return $this;
    }

    /**
     * @param array $period
     * @param User $user
     * @return int
     */
    private function calculateUserSalary(array $period, User $user): int
    {
        return $this->salaryStrategy->calc($period, $user);
    }

    private function saveSalary(Collection $userSalary)
    {
        return true;
    }
}
